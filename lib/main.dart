import 'package:flutter/material.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test',
      theme: ThemeData.dark(),
      home: StackContainer(),
    );
  }
}

class StackContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar Stack and Container'),
      ),
      body: Container(
        alignment: Alignment.center,
        color: Colors.white12,
        child: Stack(
          alignment: AlignmentDirectional.center,
          children:[
            CustomContainer(
              containerWidth: 300,
              containerHeight: 300,
              colorContainer: Colors.red,
              textChild: '20',
              colorText: Colors.white,
              textFontSize: 18,
              alignmentTextX: 0.00,
              alignmentTextY: 0.98,
            ),
            CustomContainer(
              containerWidth: 250,
              containerHeight: 250,
              colorContainer: Colors.white,
              textChild: '40',
              colorText: Colors.black,
              textFontSize: 18,
              alignmentTextX: 0.00,
              alignmentTextY: 0.96,
            ),
            CustomContainer(
              containerWidth: 190,
              containerHeight: 190,
              colorContainer: Colors.red,
              textChild: '60',
              colorText: Colors.white,
              textFontSize: 18,
              alignmentTextX: 0.00,
              alignmentTextY: 0.94,
            ),
            CustomContainer(
              containerWidth: 130,
              containerHeight: 130,
              colorContainer: Colors.white,
              textChild: '80',
              colorText: Colors.black,
              textFontSize: 18,
              alignmentTextX: 0.00,
              alignmentTextY: 0.90,
            ),
            CustomContainer(
              containerWidth: 70,
              containerHeight: 70,
              colorContainer: Colors.red,
              textChild: '100',
              colorText: Colors.white,
              textFontSize: 18,
              alignmentTextX: -0.10,
              alignmentTextY: 0.10,
            ),
          ],
        ),
      ),
    );
  }
}

class CustomContainer extends StatelessWidget {
  final double containerWidth;
  final double containerHeight;
  final Color colorContainer;

  final String textChild;
  final Color colorText;
  final double textFontSize;

  final double alignmentTextX;
  final double alignmentTextY;

  CustomContainer({
    this.containerWidth,
    this.containerHeight,
    this.colorContainer,
    this.textChild,
    this.colorText,
    this.textFontSize,
    this.alignmentTextX,
    this.alignmentTextY,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: containerWidth,
      height: containerHeight,
      decoration: BoxDecoration(
        color: colorContainer,
        shape: BoxShape.circle,
      ),
      child: Align(
        alignment: Alignment(alignmentTextX, alignmentTextY),
        child: Text(
          '$textChild',
          style: TextStyle(
            fontSize: textFontSize,
            color: colorText,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
